# Versión 1.10 - 2020.08.11

- Guardado de frames de atascos
- Guardado de velocidades y energías
- Relocalización del punto origen de rayos para detectar arcos

# Versión 1.9 - 2020.08.05

- Cambios en los mensajes del log. Incorpora el detalle de la cantidad
  de granos removidos por tipo.
- Más limpia la salida del log.
- Cambios en la búsqueda de arcos: analizo todos los granos hasta una altura
  menor a 5 veces el máximo diámetro.

# Versión 1.8 - 2020.07.28

- Cambio en la implementación del ruido: es el mismo en cada partícula
  durante todos los pasos de aplicación del impulso

# Versión 1.7 - 2020.07.14

- Emprolijamiento del código fuente
- El ruido ahora se puede aplicar durante un número establecido de 
  pasos consecutivos

# Versión 1.6 - 2020.07.18

- Actualización de biblioteca Box2D

# Versión 1.5 - 2019.06.27

- Cambios en detección y reinyección de granos, 
 
# Versión 1.4 - 2019.04.30

- Ruido completamente random por partícula

# Versión 1.3 

- Silo con tapa antes de iniciar el flujo.
 
 # Versión 1.2 

 - Incoporación de fricción con la base y modificación
   de la altura de reinyección.
  
 # Versión 1.1 

 - Corregida la salida para graficar porlígonos
 
 # Versión 1.0 

 - Versión inicial (con salida errónea para polígonos)
