/** \mainpage silo
 * \brief Simulación de descarga de silo en 2D con partículas magnéticas.
 * 
 * Programa que implementa la biblioteca Box2D para simular la
 * descarga de un silo conteniendo material granular con algunos 
 * granos magnéticos.
 *
 * \author Manuel Carlevaro <manuel@iflysib.unlp.edu.ar>
 *
 * \version 1.10 
 *
 * \date 2020.08.11
 *
 * */


#include <iostream>
using namespace std;
#include <box2d/box2d.h>
#include "globalSetup.hpp"
#include <string>
#include <cmath>
#include <vector>
using std::sin; using std::cos; using std::fabs;
#include <iomanip>
#include <sstream>
#include <cstdlib>
using std::exit;
#include "siloAux.hpp"
#include "random.hpp"

#define PI 3.1415926
int32 rayCastHit;

class MyRayCastCallback : public b2RayCastCallback {
    float ReportFixture(b2Fixture* fixture, const b2Vec2& point, 
            const b2Vec2& normal, float fraction) {
        rayCastHit = 1;
        return 0;
    }
};


int main(int argc, char *argv[])
{
    cout << "# SiloMag" << endl;
    cout << "# v1.10 [2020.08.11]" << endl;
    string inputParFile(argv[1]);
    GlobalSetup *globalSetup = new GlobalSetup(inputParFile); 
    RandomGenerator rng(globalSetup->randomSeed);
    cout << "# Creación del sistema ..." << endl;

    // Para encontrar el arco que forma el atasco:
    MyRayCastCallback* callback = new MyRayCastCallback;

    // Definición del mundo
    //b2Vec2 gravedad(0.0f, globalSetup->g );
    b2Vec2 gravedad(0.0f, globalSetup->g * sin(globalSetup->theta * PI 
                / 180.0));
    b2World world(gravedad);
    cout << "#\t- Objeto mundo creado." << endl;
    
     
    // Definición del contenedor
    b2BodyDef bd;
    bd.position.Set(0.0f, 0.0f);
    bd.type = b2_staticBody;
    b2Body *silo = world.CreateBody(&bd);
    b2Vec2 cont[6];
    cont[0].Set(globalSetup->caja.Rs, 0.0f);
    cont[1].Set(globalSetup->caja.R, globalSetup->caja.Ht);
    cont[2].Set(globalSetup->caja.R, globalSetup->caja.Ht + 
            globalSetup->caja.Hs);
    cont[3].Set(-globalSetup->caja.R, globalSetup->caja.Ht + 
            globalSetup->caja.Hs);
    cont[4].Set(-globalSetup->caja.R, globalSetup->caja.Ht);
    cont[5].Set(-globalSetup->caja.Rs, 0.0f);
    b2ChainShape siloShape;
    siloShape.CreateChain(cont,6);
    b2FixtureDef siloFix;
    siloFix.shape = &siloShape;
    siloFix.density = 0.0f;
    siloFix.friction = globalSetup->caja.fric;
    silo->CreateFixture(&siloFix);
    BodyData *siloD = new BodyData;
    siloD->isGrain = false;
    siloD->isMag = false;
    siloD->nLados = 5;
    siloD->gID = -10;
    siloD->m = 0.0;
    silo->SetUserData(siloD);
    cout << "#\t- Silo creado." << endl;

    // Tapa	
    b2BodyDef tapad;
    tapad.position.Set(0.0f, 0.0f);
    tapad.type = b2_staticBody;
    b2Body *tapa = world.CreateBody(&tapad);
    b2Vec2 v1(-0.95 * globalSetup->caja.R, 0.0f);
    b2Vec2 v2(0.95 * globalSetup->caja.R, 0.0f);
    b2EdgeShape tapaf;
    tapaf.Set(v1, v2);
    b2FixtureDef tapaFix;
    tapaFix.shape = &tapaf;
    tapaFix.density = 0.0f;
    tapaFix.friction = globalSetup->caja.fric;
    tapa->CreateFixture(&tapaFix);
    BodyData *tapaD = new BodyData;
    tapaD->isGrain = false;
    tapaD->isMag = false;
    tapaD->nLados = 1;
    tapaD->gID = -11;
    tapaD->m = 0.0;
    tapa->SetUserData(tapaD);
    cout << "#\t- Tapa creada." << endl;
    
    // Generación de granos.
    float siloInf,siloSup,siloIzq,siloDer,x,y;
    float maxDiam = 0.0f;
    float mg = 0.0;
    int noTotGranos = 0;
    siloInf = 1.5f * globalSetup->caja.Ht;
    siloSup = 0.95f * (globalSetup->caja.Ht + globalSetup->caja.Hs);
    siloIzq = -globalSetup->caja.R * 0.75;
    siloDer = globalSetup->caja.R * 0.75;

    BodyData **gInfo;
    gInfo = new BodyData*[globalSetup->noTipoGranos];
    
    int contGid = 1;
    int *sumaTipo = new int[globalSetup->noTipoGranos] {};
    int *sumaTipoArco = new int[globalSetup->noTipoGranos] {};

    for (int i = 0; i < globalSetup->noTipoGranos; i++) { // Loop sobre tipos 
        gInfo[i] = new BodyData[globalSetup->granos[i]->noGranos]; // de granos
        for (int j = 0; j < globalSetup->granos[i]->noGranos; j++) { // Loop 
            // sobre el número de granos de cada tipo.
            x = rng.getAB(siloIzq,siloDer);
            y = rng.getAB(siloInf,siloSup);
            gInfo[i][j].tipo = i;
            gInfo[i][j].isGrain = true;
            gInfo[i][j].isIn = true;
            gInfo[i][j].isMag = (abs(globalSetup->granos[i]->m) > 1.0E-8 
                    ? true : false);
            gInfo[i][j].m = globalSetup->granos[i]->m;
            gInfo[i][j].nLados = globalSetup->granos[i]->nLados;
            gInfo[i][j].f.Set(0.0f, 0.0f);
            gInfo[i][j].fi.Set(0.0f, 0.0f);
            gInfo[i][j].gID = contGid++;
            b2BodyDef bd;
            bd.type = b2_dynamicBody;
            bd.allowSleep = true;
            bd.bullet = globalSetup->isBullet;
            bd.position.Set(x,y);
            bd.angle = rng.getAB(-b2_pi,b2_pi);
            b2Body* grain = world.CreateBody(&bd);
            grain->SetUserData(&gInfo[i][j]);
            if (globalSetup->granos[i]->radio > maxDiam) 
                maxDiam = globalSetup->granos[i]->radio;
            if (globalSetup->granos[i]->nLados == 1) {
                b2CircleShape circle;
                circle.m_radius = globalSetup->granos[i]->radio;
                b2FixtureDef fixDef;
                fixDef.shape = &circle;
                fixDef.density = globalSetup->granos[i]->dens;
                fixDef.friction = globalSetup->granos[i]->fric;
                fixDef.restitution = globalSetup->granos[i]->rest;
                grain->CreateFixture(&fixDef);
                mg += grain->GetMass();
                if (j == 0) cout << "#\t- Grano de tipo " << i 
                    << " creado con masa " << grain->GetMass() 
                        << " kg." << endl;
            }
            else {
                b2PolygonShape poly;
                int32 vertexCount = globalSetup->granos[i]->nLados;
                b2Vec2 vertices[8];
                for (int k = 0; k < globalSetup->granos[i]->nLados; k++) 
                    vertices[k].Set(globalSetup->granos[i]->vertices[k][0],
                            globalSetup->granos[i]->vertices[k][1]);
                poly.Set(vertices,vertexCount);
                b2FixtureDef fixDef;
                fixDef.shape = &poly;
                fixDef.density = globalSetup->granos[i]->dens;
                fixDef.friction = globalSetup->granos[i]->fric;
                fixDef.restitution = globalSetup->granos[i]->rest;
                grain->CreateFixture(&fixDef);
                mg += grain->GetMass();
                if (j == 0) cout << "#\t- Grano de tipo " << i + 1 
                    << " creado con masa " << grain->GetMass() 
                        << " kg." << endl;
            }
            noTotGranos++;
        } // Fin loop sobre el número de granos de cada tipo.
    } // Fin loop sobre tipo de granos	
    cout << "#\t- Número total de granos = " << noTotGranos << endl;
    cout << "#\t- Masa total de granos = " << mg << " kg."<< endl;
    maxDiam *= 2.0;
    cout << "#\t- Límite de reinyección = " << -maxDiam << " m." << endl;

    // Preparamos los parámetros de la simulación. 
    float timeStep = globalSetup->tStep;
    float timeS = 0.0;
    int32 pIterations = globalSetup->pIter;
    int32 vIterations = globalSetup->vIter;

    cout << "# Inicio de la simulación..." << endl;
    b2Vec2 pos, avec, vtmp, unifNoise; 
    float angle = 0.0, vtmpM, maxY;
    float friction_force;
    double noiseInt, noiseAng;
    std::ofstream fileF;
    int nGranosDesc = 0;
    BodyData *infGr;
    int deltaG = 0;
    int32 countAtaSteps = 0; /*!< pasos con el silo atascado */
    int32 desAnt = 0; /*!< Descarga en el paso anterior */
    double yO = globalSetup->caja.Rs * globalSetup->caja.Ht 
        / (globalSetup->caja.Rs - globalSetup->caja.R);
    cout << "# Origen de rayos: 0.0, " << yO << endl;
    b2Vec2 O(0.0f, yO); /*!< Origen de coordenadas */
    vector<int> archGrainIndex;
    bool found;
    bool saveEV = (globalSetup->veFreq > 0 ? true : false);
    std::ofstream fileVE;
    if (saveEV) {
        fileVE.open(globalSetup->veFile.c_str());
        fileVE << "#time |v| v^2 ";
        for (int i = 0; i < globalSetup->noTipoGranos; ++i) {
            fileVE << "eK_" << i + 1 << " ";
        }
        fileVE << "eMag" << endl;
    }

    // Llenado del silo y decantación
    cout << "# Llenado del silo y decantación..." << endl;
    int paso = 0;
    while (timeS < globalSetup->tFill) {
        // Cálculo y aplicación de fuerzas magnéticas y fricción con la base 
        // Nota: g = 1 y la normal es siempre la masa ( * 1 kg / (m s^2) )
        setMagneticForces(&world);
        for (b2Body *bd = world.GetBodyList(); bd; bd = bd->GetNext()) {
            infGr = (BodyData*) (bd->GetUserData());
            bd->ApplyForce(infGr->f, bd->GetWorldCenter(), true);
            if (bd->GetType() == b2_dynamicBody) {
                vtmp = bd->GetLinearVelocity();
                vtmpM = vtmp.Length();
                if (vtmpM > 1.0e-3) {
                    friction_force = globalSetup->caja.fricB * bd->GetMass();
                    vtmp.Normalize();
                    vtmp = -vtmp;
                    vtmp *= friction_force;
                    bd->ApplyForceToCenter(vtmp, true);
                }
                else {
                    bd->SetLinearVelocity(b2Vec2(0.0f, 0.0f));
                    bd->SetAngularVelocity(0.0f);
                }
            }
        }
        world.Step(timeStep,pIterations,vIterations);
        paso++;
        timeS += timeStep;
    }
    cout << "# ... tiempo de tapa (" << globalSetup->tFill << " s) finalizado." 
        << endl;
    world.DestroyBody(tapa);
    tapa = NULL;
    cout << "# Tapa removida, inicio de la descarga." << endl;

    timeS = 0.0f;
    paso = 0;
    
    bool saveFrm = (globalSetup->saveFrameFreq > 0 ? true : false);
    bool saveFlux = (globalSetup->fluxFreq > 0 ? true : false);
    std::ofstream fileFlux;
    if (saveFlux) {
        fileFlux.open((globalSetup->fluxFile).c_str());
        fileFlux << "#grainDesc type time ";
        for (int i = 0; i < globalSetup->noTipoGranos; ++i) {
            fileFlux << "totalType_" << i + 1 << " ";
        }
        fileFlux << " Total" << endl;
    }
    // Guardo configuración inicial
    string foutName = globalSetup->preFrameFile+ "_" + n2s(paso) + ".xy";
    fileF.open(foutName.c_str());
    saveFrame(&fileF, &world);
    fileF.close();
    cout << "Frame " << paso << " guardado en " << timeS << endl;

    bool impulsos = false;
    int imp_duration = 0;
    double maxAltArco = 10.0 * maxDiam; // Máxima altura para buscar arcos
    // Inicio el avance del tiempo en la simulación
    while (timeS < globalSetup->tMax) {
        // Detección de la altura máxima de granos 
        maxY = 0.0f;
        for (b2Body *bd = world.GetBodyList(); bd; bd = bd->GetNext()) {
           if (bd->GetType() != b2_dynamicBody) continue;
           pos = bd->GetPosition();
           if (pos.y > maxY) maxY = pos.y;
        }      

        // Verifico si hay atasco   
        if (countAtaSteps * globalSetup->tStep 
                > globalSetup->tiempoEsperaAtasco) {
            for (int i = 0; i < globalSetup->noTipoGranos; ++i) {
                sumaTipoArco[i] = 0;
            }
            // Detecto granos en el arco:
            //cout << timeS << " s -> Atasco: " << countAtaSteps << " " 
                //<< countAtaSteps * globalSetup->tStep << endl;
            cout << timeS << " s -> Atasco." << endl;
            if (globalSetup->saveAtascoFrm) {
                string foutName = globalSetup->preAtc+ "_" 
                    + n2s(paso) + ".xy";
                fileF.open(foutName.c_str());
                saveFrame(&fileF, &world);
                fileF.close();
            }
            archGrainIndex.clear();
            for (b2Body *bd = world.GetBodyList(); bd; bd = bd->GetNext()) {
                infGr = (BodyData*)(bd->GetUserData());
                pos = bd->GetPosition();
                //if (infGr->isGrain && infGr->isIn  // Granos sobre el orificio 
                        //&& abs(pos.x) < globalSetup->caja.Rs) {
                if (infGr->isGrain && infGr->isIn
                        && pos.y < maxAltArco) { //Todos los granos < maxAltArco
                    rayCastHit = 0;
                    world.RayCast(callback, pos, O);
                    if (rayCastHit == 0) { // No hay fixtures entre bd y O 
                        // >> está en el arco
                        archGrainIndex.push_back(infGr->gID);
                        sumaTipoArco[infGr->tipo]++;
                    }
                }
            }
            //cout << rayCastHit << endl;
            found = false;
            for (b2Body *bd = world.GetBodyList(); bd; bd = bd->GetNext()) {
                infGr = (BodyData*) (bd->GetUserData());
                for (auto & id : archGrainIndex) {
                    if (infGr->gID == id) {
                        found = true;
                        break;
                    }
                    else found = false;
                }
                if (found) {
                    pos = bd->GetPosition();
                    x = pos.x;
                    y = -pos.y; // Ubico el grano del arco debajo del orificio 
                    // de salida
                    b2Vec2 newPos(x,y);
                    bd->SetTransform(newPos, angle);
                    bd->SetLinearVelocity(b2Vec2(0.0f, 0.0f));
                }
            }
            cout << "Arco removido: " << archGrainIndex.size() 
                << " granos ";
                for (int i = 0; i < globalSetup->noTipoGranos; ++i) {
                    cout << "tipo " << i + 1 << ": " << sumaTipoArco[i] << " ";
                }
                cout << endl;
            countAtaSteps = 0;
            desAnt = 0;
        }


        // Cálculo de descarga y reinyección
        deltaG = countDesc(&world, sumaTipo, globalSetup->noTipoGranos, 
                globalSetup->tStep, paso, fileFlux);
        nGranosDesc += deltaG;
        if (deltaG == desAnt) {
           countAtaSteps++; 
        }
        else {
            desAnt = deltaG;
            countAtaSteps = 0;
        }
        for (b2Body *bd = world.GetBodyList(); bd; bd = bd->GetNext()) {
            infGr = (BodyData*) (bd->GetUserData());
            pos = bd->GetPosition();
            angle = bd->GetAngle();
            if ((infGr->isGrain) && !infGr->isIn && (pos.y < -3.0 * maxDiam)) {
                x = rng.getAB(-0.8 * globalSetup->caja.R, 
                        0.8 * globalSetup->caja.R);
                y = maxY + 0.1 * maxDiam;  // FIXME: Corregir esto
                if (y > 0.9 * (globalSetup->caja.Ht + globalSetup->caja.Hs))
                    y = 0.9 * (globalSetup->caja.Ht + globalSetup->caja.Hs);
                b2Vec2 newPos(x,y);
                bd->SetTransform(newPos, angle);
                bd->SetLinearVelocity(b2Vec2(0.0f, 0.0f));
                infGr->isIn = true;
            }
        }


        // Cálculo y aplicación de fuerzas magnéticas y fricción con la base 
        // Nota 1: g = 1 y la normal es siempre la masa ( * 1 kg / (m s^2) )
        // Nota 2: NO se aplica la fricción con la base
        setMagneticForces(&world);
        for (b2Body *bd = world.GetBodyList(); bd; bd = bd->GetNext()) {
            infGr = (BodyData*) (bd->GetUserData());
            bd->ApplyForce(infGr->f, bd->GetWorldCenter(), true);
        }

        // Cálculo y aplicación del ruido
        if (globalSetup->noiseFreq && !(paso % globalSetup->noiseFreq)) {
            impulsos = true;
            imp_duration = globalSetup->noiseDuration;
            noiseInt = rng.get01() * globalSetup->noise;
            noiseAng = rng.get01() * 2.0f * PI;
            unifNoise.Set(noiseInt * cos(noiseAng), noiseInt * sin(noiseAng));
            for (b2Body *bd = world.GetBodyList(); bd; bd = bd->GetNext()) {
                infGr = (BodyData*) (bd->GetUserData());
                if (infGr->isGrain) {
                    //bd->ApplyForce(avec, bd->GetWorldCenter(), true);
                    noiseInt = rng.get01() * globalSetup->noise;
                    noiseAng = rng.get01() * 2.0f * PI;
                    avec.Set(noiseInt * cos(noiseAng), 
                            noiseInt * sin(noiseAng));
                    avec += unifNoise;
                    infGr->fi = avec;
                }
            }
        }
        if (impulsos) {
            for (b2Body *bd = world.GetBodyList(); bd; bd = bd->GetNext()) {
                infGr = (BodyData*) (bd->GetUserData());
                if (infGr->isGrain) {
                    bd->ApplyLinearImpulseToCenter(infGr->fi, true);
                }
            }
            imp_duration--;
            if (!imp_duration) impulsos = false;
        }

        // Imprimo velocidades y energías
        if (saveEV && !(paso % globalSetup->veFreq)) {
            printVE(timeS, &fileVE, &world, globalSetup);
        }
        
        world.Step(timeStep,pIterations,vIterations);
        paso++;
        timeS += timeStep;

        // Si es necesario, guardo el frame para graficar
        if (saveFrm && !(paso % globalSetup->saveFrameFreq)) {
            string foutName = globalSetup->preFrameFile+ "_" 
                + n2s(paso) + ".xy";
            fileF.open(foutName.c_str());
            saveFrame(&fileF, &world);
            fileF.close();
            //cout << "Frame " << paso << " guardado en " << timeS << endl;
        }
        if (timeS > globalSetup->tMax) {
            cout << "# Máximo tiempo de simulación alcanzado." << endl;
            break;
        }
    }
    cout << "Descarga final: " << nGranosDesc << endl;
    for (int i = 0; i < globalSetup->noTipoGranos; ++i){
        cout << "Granos de tipo " << i + 1 << ": " << sumaTipo[i] << endl;
    }
    cout << "Simulación finalizada." << endl;

    
    // Clean up
    if (saveFlux) fileFlux.close();
    if (saveEV) fileVE.close();
    delete globalSetup;
    delete siloD;
    delete [] gInfo;
    delete [] sumaTipo;
    delete callback;

    return 0;
}	

