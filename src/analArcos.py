#!/usr/bin/env python3

import glob

files = []
for f in glob.glob('*.dat'):
    files.append(f)
files.sort()

resultados = []
for fil in files:
    fin = open(fil, 'r')
    data = fin.readlines()
    fin.close()
    sfile = fil[:-4]
    lsfile = sfile.split('-')
    n = int(lsfile[1][1:])
    m = int(lsfile[2][1:])
    rdc = float(lsfile[3][1:])
    final = False
    n_arcos = 0
    d_final = 0
    g_t1 = 0
    g_t2 = 0
    for fila in data:
        if fila[0] == "#":
            continue
        f = fila.split()
        if len(f) == 10 and f[0] == 'Arco':
            n_arcos += 1
        if len(f) == 3 and f[0] == 'Descarga' and f[1] == 'final:':
            d_final = int(f[2])
        if len(f) == 5 and f[0] == 'Granos' and f[3] == '1:':
            g_t1 = int(f[4])
        if len(f) == 5 and f[0] == 'Granos' and f[3] == '2:':
            g_t2 = int(f[4])
        if len(f) == 2 and f[1] == 'finalizada.':
            final = True
        if final:
            resultados.append( (n, m, rdc, n_arcos, g_t1, g_t2, d_final) )
            n_arcos = 0
            d_final = 0
            g_t1 = 0
            g_t2 = 0
            final = False
    data = []

print('n,m,r,n_arcos,g_t1,g_t2,d_final')
for r in resultados:
    n, m, rdc, na, t1, t2, d = r
    print(f'{n},{m},{rdc},{na},{t1},{t2},{d}')
