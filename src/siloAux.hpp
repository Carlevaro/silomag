/*! \file siloAux.hpp
 * \brief Archivo de cabecera para funciones auxiliares
 *
 * \author Manuel Carlevaro <manuel@iflysib.unlp.edu.ar>
 * \version 1.0
 * \date 20018.12.14
 */

#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <box2d/box2d.h>
#include "globalSetup.hpp"
//#include <cmath>
//using std::sqrt;

/*! Convierte un número en una string de ancho fijo
 * y rellena de ceros, para enumerar frames secuencialmente
 * \param int num
 * \return std::string
 */
std::string n2s(int num);

/*! Detecta si el sistema está activo
 * \param b2World* w 
 * \return bool
 */
bool isActive(b2World *w);

/*! Escribe en el archivo de salida las coordenadas de las partículas
 * \param std::ofstream* ff
 * \param b2Word* w
 * \return void
 */
void savePart(std::ofstream *ff, b2World *w);

/*! Escribe todas las coordenadas necesarias para generar imágenes
 * y posteriores animaciones
 * \param std::ofstream* ff
 * \param b2World* w
 * \return void
 */
void saveFrame(std::ofstream *ff, b2World *w);

/*! Devuelve la cantidad de granos descargados
 * \param b2World* w
 * \param int* st (suma por tipo de granos)
 * \para int cantidad de tipo de granos
 * \param double tStep delta_t de integegración
 * \param paso paso actual de la simulación
 * \param ofstream archivo de salida del flujo
 * \return int
 */
int countDesc(b2World *w, int *st, int nTipos, double tStep, int paso, std::ofstream &fluxFile);

/*! Establece la fuerza magnética sobre los granos
 * \param b2World* w
 * \return void
 */
void setMagneticForces(b2World *w);

/*! Imprime las velocidades y energías
 * \param float timeS : tiempo de la simulación
 * \param ofstream* fileVE : archivo de salida
 * \param b2Word* w : mundo
 * \return void
 */
void printVE(float timeS, std::ofstream *ff, b2World *w, 
        const GlobalSetup* gs);
