# siloMag

Simulación con Box2D de la descarga de un silo con granos magnéticos.
Los parámetros de la simulación se establecen en un archivo de texto como 
el adjunto (params.ini). Las dimensiones del silo se establecen según
el plano adjunto en /doc/contenedor.pdf.

# Requerimientos:
1. Box2D
2. Matplotlib
3. ffmpeg

# Compilación
    make
Es necesario indicar en el Makefile la ruta hacia la librería libBox2D.a y
también el camino hacia Box2D.h

# Documentación
La documentación del código se genera en el directorio raíz con:

    doxygen Doxyfile

Esto genera la documentación en html en el directorio /doc.

# Uso:
    ./siloMag params.ini &> salida.out

# Generación de animación:
Este proceso consiste en dos pasos: 1) Generar los archivos de imágenes, 
2) concatenar las imágenes en un video.

1. Generación de imágenes:

    ./makePreMovie.py -f [prefijo de nombre de archivos .xy]

Ejemplo:

    ./makePreMovie.py -f frm

2. Generación de video:

    ./dovideo.sh [nombre] [framerate]

Ejemplo:

    ./dovideo.sh dd-100-20 10

## Ejemplos

1. 200 discos no magnéticos de radio 0.5 y 20 con m positivo y radio 0.5

[![200 +20](http://img.youtube.com/vi/qiRPcyhVVb0/0.jpg)](https://youtu.be/qiRPcyhVVb0)

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/qiRPcyhVVb0" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

2. 200 discos no magnéticos de radio 0.5, 20 con m > 0 y radio 0.7 y 20 con m positivo y radio 0.3


[![200 +20G -20S](http://img.youtube.com/vi/q00r3O3C5DI/0.jpg)](https://youtu.be/q00r3O3C5DI)


2. 200 discos no magnéticos de radio 0.5, 20 con m positivo y radio 0.7 y 20 con m negativo y radio 0.7


[![200 +20G -20G](http://img.youtube.com/vi/jlNJNhceW0c/0.jpg)](https://youtu.be/jlNJNhceW0c)

3. 200 discos no magéticos de radio 0.15 y 20 con m positivo de forma cuadrada y radio 0.17.

[![200 +20G -20G](http://img.youtube.com/vi/Z2HAaTIfC7I/0.jpg)](https://youtu.be/Z2HAaTIfC7I)

4. 200 discos no magéticos de radio 0.15, 20 con m positivo de forma cuadrada y radio 0.17 y 20 con m negativo de forma triangular y radio 0.17.

[![200 +20G -20G](http://img.youtube.com/vi/0aznq3zD5W8/0.jpg)](https://youtu.be/0aznq3zD5W8)

